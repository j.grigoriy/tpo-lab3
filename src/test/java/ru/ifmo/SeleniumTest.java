package ru.ifmo;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import static org.junit.jupiter.api.Assertions.*;

public class SeleniumTest {

    // java -Dwebdriver.gecko.driver="/Users/grigoriy/browser_drivers/geckodriver" -Dwebdriver.chrome.driver="/Users/grigoriy/browser_drivers/chromedriver" -jar /Users/grigoriy/Desktop/Sem6/TPO/Lab3/selenium_server/selenium-server-standalone-3.141.59.jar

    @BeforeAll
    static void prepareDrivers() {
        Utils.prepareDrivers();
    }

    @Test
    void testDriver() {
        Utils.getDrivers().forEach(this::executeWithCapabilities);
    }

    private void executeWithCapabilities(WebDriver driver) {
        driver.get(Utils.BASE_URL);
        String title = driver.getTitle();
        assertEquals("Бесплатный эротический видеочат с онлайн-шоу", title);
        driver.quit();
    }
}
