package ru.ifmo.usecase;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import ru.ifmo.Utils;
import ru.ifmo.model.AdultConfirmationPage;

import java.util.List;

public class AdultConfirmationTest {

    @BeforeAll
    public static void prepareDrivers() {
        Utils.prepareDrivers();
    }

    @Test
    void acceptAdultConfirmationTest() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(webDriver -> {
            webDriver.get(Utils.BASE_URL);
            AdultConfirmationPage page = new AdultConfirmationPage(webDriver);
            page.acceptAdultConfirmation();
        });
        drivers.forEach(WebDriver::quit);
    }

    @Test
    void dismissAdultConfirmationTest() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(webDriver -> {
            webDriver.get(Utils.BASE_URL);
            AdultConfirmationPage page = new AdultConfirmationPage(webDriver);
            page.dismissAdultConfirmation();
        });
        drivers.forEach(WebDriver::quit);
    }
}
